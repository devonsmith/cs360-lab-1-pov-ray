# README #

This is the first lab for CS360: Programming Languages at Western Oregon University.

### What is this repository for? ###

* CS360 Lab One - POV-Ray
* This file is a POV-Ray file that is intended for rendering a scene with POV-Ray.
* Requires POV-Ray for rendering

### How do I get set up? ###

* Install POV-Ray
* Download the source