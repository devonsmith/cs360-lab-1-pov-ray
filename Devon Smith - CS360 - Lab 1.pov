/*****************************************************************************
//
// CS360 - Lab One
// Western Oregon University
// Devon Smith
// October 2016
//
//
// Project Description: 
// Create a project using the Scene Description Language in POV Ray.
// The project must include the following:
//
//              * 3 Objects (Planes, box, pyramids) 
//              *Include two of the following operations:
//                   1) A Union: in this case I used a merge which is a union
//                      that removed all intersection geometry.
//                   2) An Intersection: Used in pyramids
//                   3) A Difference: used to cut out window
//              * A camera
//              * A light source
//
//
// Initial Thoughts: I think I'm going to try and create an arwing from
// the Nintendo game Starfox. The arwing is copyright of the Nintendo Co. ltd.
// for educational use only.
//
// Current Version: This is just the main body of the aircraft. More pieces to 
// be added at a later date.
//
*****************************************************************************/

// Camera
camera{ perspective location <10.0,10.0,10.0>look_at <0,0,0> }


// Light Source
light_source{
    // where is the light source in 3D space?
    <4,10,10>
    // What color should the light be?                                 
    color rgb<2, 2, 2>
}


// insert a plane for the floor
plane{
      // Where is this plane located?
      // Location, how far down below the base plane
      <0,2,0>, -3.5
      // Planes need a color, white?
      pigment {rgb <0,0.4,0.15>}

}

/**********************************************************************************************
// Test Object Primitives (Cube)
***********************************************************************************************/
// This test object came from the lab example text file lab1.pdf from Western Oregon Univerity.

/*
// create a box object  

box {
 <-0.5, -0.5, -0.5>,<0.5,0.5,0.5>
 // When using a specular or semi-translucent object you need to specify it's interior property
 interior {ior 1.3 }
 // What color is the box? Lime Green, yes?
 pigment { rgbf <0,0.95,0,1> }
 // How should light behave when it hits this object?
 photons { target reflection on refraction on }

} 

*/

// Note: The math for the tetrahedron provided by Friedrich A. Lohmueller

/*
// Let's make a tetrahedron!
object {

intersection {
   plane {-y,0.5}
   plane {-z,0.5 rotate <19.47,    0, 0>}
   plane {-z,0.5 rotate <19.47, -120, 0>}
   plane {-z,0.5 rotate <19.47,  120, 0>}
   
   bounded_by {sphere {0, 3}
   }
   pigment {rgbf <0,0.95,0,0.5>}  
   photons { target reflection on refraction on}
  }
  
  
*/


/*
// Let's make an octahedron!
object{
 intersection 
  {
   plane { z, 1 rotate < 35.26438968275, 0, 0>}
   plane { z, 1 rotate <-35.26438968275, 0, 0>}
   plane {-z, 1 rotate < 35.26438968275, 0, 0>}
   plane {-z, 1 rotate <-35.26438968275, 0, 0>}
   
   plane { x, 1 rotate <0, 0, -35.26438968275>}
   plane { x, 1 rotate <0, 0,  35.26438968275>}
   plane {-x, 1 rotate <0, 0, -35.26438968275>}
   plane {-x, 1 rotate <0, 0,  35.26438968275>}
  
   bounded_by {sphere {0, 1.7321}
   } 
   pigment {rgbf <0,0.95,0,0.5>}  
   photons { target reflection on refraction on}
}


}*/

// Let's try merging to tetrahedrons.
// Scale and translate code examples were provided by the POV Ray documentation

/*****************************************************************************
// How to use a rotate:                                                       
*****************************************************************************/
// Rotate is done via degrees around and axis.
// rotate <x,y,z>
//
// examples for clarity:
//
// rotate <0, 30, 0>  // 30 degrees around Y axis then,
// rotate <-20, 0, 0> // -20 degrees around X axis then,
// rotate <0, 0, 10>  // 10 degrees around Z axis.

//Note these examples were from the POV Ray Documentation
/****************************************************************************/

/*****************************************************************************
// How to use a scale:                                                        
*****************************************************************************/
// scale <x, y, z>
// scales the object over the dimension specified. using uneven values for x, 
// y, and z will result in the object being stretched or squished.

/****************************************************************************/



/*****************************************************************************
// How to use a translate:                                                    
*****************************************************************************/
// translate <x, y, z>
// translates the object away from the point of origin.                       
/****************************************************************************/


// First let's get the tetrahedrons where I want them, so they're touching.

// Tetrahedron A //green


// Comment out for merge operation//
/*
object {

intersection {
   plane {-y,0.5}
   plane {-z,0.5 rotate <19.47,    0, 0>}
   plane {-z,0.5 rotate <19.47, -120, 0>}
   plane {-z,0.5 rotate <19.47,  120, 0>}
   
   bounded_by {sphere {0, 3}
   }
   // This one will be green like in our green Jello cube test.
   pigment {rgbf <0,0.95,0,0.5>}  
   photons { target reflection on refraction on}
   // rotate 90 degrees anti-clockwise on the z-axis
   rotate <0, 0, 90>
   //translate on the z-axis
   translate <-0.5,0,0> 

   }
}
  


// Tetrahedron B // red
object {

intersection {
   plane {-y,0.5}
   plane {-z,0.5 rotate <19.47,    0, 0>}
   plane {-z,0.5 rotate <19.47, -120, 0>}
   plane {-z,0.5 rotate <19.47,  120, 0>}
   
   bounded_by {sphere {0, 3}
   }
   // The one will be red like in the lab manual example of red Jello.
   pigment {rgbf <0.95,0,0,0.5>}  
   photons { target reflection on refraction on}
   // rotate 90 degrees clock-wise on the z-axis.
   rotate <0, 0, 270>
   //translate on the z-axis.
   translate <0.5,0,0> 
   // scale the tetrahedron to make it longer in the x-direction
   scale <3,0,0>
   
   } 
}

*/


/*
// Now let's try putting them together to make a single solid using the merge
// method. This works similar to the union operation but instead of just 
// sticking the objects together it actually removed the edges of the objects
// that are "glued".

merge{  

    // Tetrahedron A
    object {

        intersection {
            plane {-y,0.5}
            plane {-z,0.5 rotate <19.47,    0, 0>}
            plane {-z,0.5 rotate <19.47, -120, 0>}
            plane {-z,0.5 rotate <19.47,  120, 0>}
   
            bounded_by {sphere {0, 3}
        }
        // rotate the object 90 degrees anti-clockwise
        rotate <0, 0, 90>
        //translate on the z-axis
        translate <-0.5,0,0> 

    }
}

    // Tetrahedron B
    object {

        intersection {
            plane {-y,0.5}
            plane {-z,0.5 rotate <19.47,    0, 0>}
            plane {-z,0.5 rotate <19.47, -120, 0>}
            plane {-z,0.5 rotate <19.47,  120, 0>}
   
            bounded_by {sphere {0, 3}
        }
        // Rotate the object 90 degrees clockwise
        rotate <0, 0, 270>
        //translate on the z-axis.
        translate <0.5,0,0> 
        // scale the tetrahedron to make it longer in the x-direction
        scale <3,0,0>
   
        }
    }
    pigment {rgbf <0,0.95,0,0.5>}  
    photons { target reflection on refraction on}

} 

// This was a good example of merging to objects.

*/


/*
// Creating the same with a pyramid.
// In this case I'm going to create planes that have been moved 1 unit apart 
// and tilted by sqrt(2)/2 radians.

// I used the same sphere bounding box that I used in the tetrahedron example.

merge {
    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object.
        rotate <0, 60, 270>
                //translate on the x-axis.
        // When I scaled the object, it moved away from the other object.
        // I had to adjust the offset in the negative direction to merge 
        // the two pieces together. I had to move the object back 0.4 on
		// the x-axis
        translate <-0.40,0,0>
        // scale the tetrahedron to make it longer in the x-direction
        scale <5,0,0> 
        }
    }        

    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object to meet the other
        // in this case because I rotated this one to a different
        // z-axis, I need to increase the y-axis rotation.
        rotate <0, 120, 90>
        //translate on the x-axis.
        translate <-2,0,0> 
        }

    }
    pigment {rgbf <0,0.95,0,0.5>}  
    photons { target reflection on refraction on}      
}
  
*/

/*****************************************************************************
// End of Test Object
*****************************************************************************/



/*****************************************************************************
// Arwing 
*****************************************************************************/

// Main body

// This section is the metallic main body of the arwing.
difference {
merge {
    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object.
        rotate <0, 45, 270>
        //translate on the x-axis.
        // When I scaled the object, it moved away from the other object.
        // I had to adjust the offset in the negative direction to merge the
        // two pieces together. I had to move the object back 0.4 on the x-axis
        translate <-0.4,0,0>
        // scale the tetrahedron to make it longer in the x-direction
        scale <5,0,0> 
        }
    }        

    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object to meet the other
        // in this case because I rotated this one to a different
        // z-axis, I need to increase the y-axis rotation.
        rotate <0, 45, 90>
        //translate on the x-axis.
        translate <-2,0,0>
        }

    }
    // Set the color of the object to a white solid.
    pigment {rgbf <1,1,1,0>}
    // how should the light behave when it's cast on the object?  
    photons { target reflection on refraction on}
    // This changes how dark the model is with the available lights.
    // This is used to keep the main body bright without adding more
    // lights to the parse tree.
    finish {ambient 0.40 }  // TODO: Remove this and add more light sources.    
}           

// This is the box used to cut out the space for the window.
box{
   <-1.25, -1.25, -1.25>,<1.25,1.25,1.25>
   translate <0,1.75,0>
   // when using a difference the color of the object cutting away will be
   // given to any faces made by the cut. So I made this cut match the 
   // body of the arwing.
   pigment { rgb <1,1,1> }
}


}    


// This solid body acts as the window of the arwing when the window space is
// cut away.
merge {
    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object.
        rotate <0, 45, 270>
        //translate on the x-axis.
        // When I scaled the object, it moved away from the other object.
        // I had to adjust the offset in the negative direction to merge the
        // two pieces together. I had to move the object back 0.4 on the x-axis
        translate <-0.40,0,0>
        // scale the tetrahedron to make it longer in the x-direction
        scale <5,0,0> 
        }
    }        

    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object to meet the other
        // in this case because I rotated this one to a different
        // z-axis, I need to increase the y-axis rotation.
        rotate <0, 45, 90>
        //translate on the x-axis.
        translate <-2,0,0>
        }

    } 
    // what color? This was found with some trial and error.
    pigment {rgbf <0.25,0.25,1,0.85>}
    interior {ior 1.5 } // What is the interior of the solid like? 
	                    // Documentation says: 1.5 for glass. 
    // how should light react against the object when cast?
    photons { target reflection on refraction on}      
}

/*****************************************************************************
// bullet                                                                                                         
*****************************************************************************/

// this bullet object is not completely ready. As it is, the model has been 
// adjusted to ignore ambient light values however, it would be better to 
// assign it an emitting media to the object to make it work. This code is 
// here for demonstration purposes. I will update it with a proper light 
// source when I have more time

/*
merge {
    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object.
        rotate <0, 120, 90>
        //translate on the x-axis.
        // When I scaled the object, it moved away from the other object.
        // I had to adjust the offset in the negative direction to merge the
        // two pieces together.
        
        translate <-0.40,0,0>
        // scale the tetrahedron to make it longer in the x-direction
        scale <5,0,0> 
        }
    }        

    object{
        intersection {
            plane { < 1, 0,  0>, 1  rotate <  0, 0,  40.5142342>}
            plane { <-1, 0,  0>, 1  rotate <  0, 0, -40.5142342>}
            plane { < 0, 0,  1>, 1  rotate <-40.5142342, 0,   0>}
            plane { < 0, 0, -1>, 1  rotate < 40.5142342, 0,   0>}
            plane { <0, -1, 0>, 0 }
            bounded_by {sphere {0, 3} 
        }
        // Rotate the object to meet the other
        // in this case because I rotated this one to a different
        // z-axis, I need to increase the y-axis rotation.
        rotate <0, 60, 270>
        //translate on the x-axis.
        translate <-2,0,0>
        }

    } 
    // what color? This was found with some trial and error.
    pigment {rgbf <0.25,0.25,1,0.85>}
    interior {ior 1.5 } // what is the interior of the solid like? 
	                    // Documentation says: 1.5 for glass. 
    // how should light react against the object when cast?
    photons { target reflection on refraction on}
    translate <55,-5,0>
    scale <0.18, 0.18, 0.18>
    finish { ambient 4 }      
}
*/

/******************* NOT IMPLEMENTED YET ************************************/
// left wing

// right wing

//left sub-wing

// right sub-wing   

